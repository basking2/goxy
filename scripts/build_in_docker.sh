#!/bin/sh

set -xe

docker build -t "basking2/goxy:latest" .

if [ -n "$CI_COMMIT_TAG" ] && [ -n "$DOCKERHUB_API_KEY" ]; then
	# set -e above will exit if this fails.
	echo $CI_COMMIT_TAG | grep -E '^v\d+\.\d+\.\d+$' > /dev/null

	VERSION=`echo $CI_COMMIT_TAG | sed -E 's/^v(.*)$/\1/'`

	docker login --password "${DOCKERHUB_API_KEY}" -u basking2
	docker tag "basking2/goxy:latest" "basking2/goxy:${VERSION}"
	docker push "basking2/goxy:${VERSION}"
	docker push "basking2/goxy:latest"
fi
