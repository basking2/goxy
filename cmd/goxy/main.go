package main

import (
	"flag"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/basking2/goxy.v1/pkg/goxy"
)

func main() {

	var config = parseConfig()
	loadConfig("Foo")

	if len(config.Socks)+len(config.HTTP) > 0 {

		for _, v := range config.Socks {
			goxy.StartSocks(v.Bind)
		}

		for _, v := range config.HTTP {
			goxy.StartHTTP(v.Bind)
		}

		waitForKill()
	} else {
		println("Run -h for usage information.")
	}
}

func waitForKill() {
	var c = make(chan os.Signal, 1)

	signal.Notify(c, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGKILL)

	<-c
}

func parseConfig() *goxy.Config {
	var config = new(goxy.Config)

	flag.Var(&config.Socks, "socks", "Add a bind:port for a proxy.")
	flag.Var(&config.HTTP, "http", "Add a bind:port for a proxy.")

	flag.Parse()

	return config
}

func loadConfig(file string) string {

	// fmt.Printf("Loading %s.\n", file)
	return file
}
