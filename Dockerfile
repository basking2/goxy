FROM fedora:35
RUN dnf install -q -y go
COPY . /src
WORKDIR /src
RUN go build ./cmd/goxy

# FROM scratch
FROM fedora:32
EXPOSE 8080/tcp
EXPOSE 1080/tcp
COPY --from=0 /src/goxy /usr/bin
CMD [ "/usr/bin/goxy", "-socks", "0.0.0.0:1080", "-http", "0.0.0.0:8080" ]
