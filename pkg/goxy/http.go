package goxy

import (
	"errors"
	"fmt"
	"log"
	"net"
	"regexp"
	"strings"
)

// HTTP is the http proxy struct.
type HTTP struct {
	bind string
}

func runHTTPProxyConn(conn net.Conn) error {
	var buffer = make([]byte, 1024)
	var header, err = ReadStringUntilContains(conn, buffer, "\r\n\r\n")
	if err != nil {
		log.Printf("Unhandled error reading header: %s", err)
		// log.Printf("Header is %s  ----  %s", header, buffer)
		return err
	}

	if strings.HasPrefix(header, "CONNECT") {
		return handleConnect(conn, header)
	} else if host, err := parseHostHeader(header); err != nil {
		log.Printf("Unhandled error parsing host from header: %s", err)
		// log.Printf("<<%s>>", header)
		conn.Write([]byte("HTTP/1.1 400 OK\r\nContent-Length: 0\r\n\r\n"))
		conn.Close()
		return err
	} else {
		return handleOther(conn, host, header)
	}
}

func handleOther(conn net.Conn, host string, header string) error {
	header = cleanHTTPStatusLine(header)

	server, err := net.Dial("tcp", host)
	if err == nil {
		_, err := server.Write([]byte(header))
		if err != nil {
			log.Printf("Unhandled error writing to %s: %s", host, err)
			conn.Write([]byte("HTTP/1.1 400 OK\r\nContent-Length: 0\r\n\r\n"))
			conn.Close()
			return err
		}

		var proxyConn = NewProxyConn(conn, server)

		return proxyConn.DoProxy()
	}

	log.Printf("Unhandled error connecting to %s: %s", host, err)
	conn.Write([]byte("HTTP/1.1 400 OK\r\nContent-Length: 0\r\n\r\n"))
	conn.Close()
	return err
}

// For HTTP verb lines that have the protocol and host in them, remove it and leave only the URL path.
func cleanHTTPStatusLine(headers string) string {
	return regexp.MustCompile("^([\\w]+)[ \t]+http://[^/\r\n]+/").ReplaceAllString(headers, "$1 /")
}

func handleConnect(conn net.Conn, header string) error {
	if match := regexp.MustCompile("[Cc][Oo][Nn][Nn][Ee][Cc][Tt] (\\S+) HTTP/.*").FindStringSubmatch(header); match != nil {
		host, err := parseHostHeader(header)

		if err != nil {
			log.Println(err)
			return err
		}

		server, err := net.Dial("tcp", host)

		if err != nil {
			conn.Close()
			log.Println(err)
			return err
		}

		conn.Write([]byte("HTTP/1.1 200 OK\r\nContent-Length: 0\r\n\r\n"))

		var proxyConn = NewProxyConn(conn, server)

		return proxyConn.DoProxy()

	}

	return fmt.Errorf("malformed connect request")
}

func parseHostHeader(head string) (string, error) {
	if match := regexp.MustCompile("[Hh][Oo][Ss][Tt]:\\s*([^\r\n\\s]*)(?mU:\\s*$)").FindStringSubmatch(head); match != nil {
		if _, _, err := net.SplitHostPort(match[1]); err != nil {
			return fmt.Sprintf("%s:80", match[1]), nil
		}

		return match[1], nil
	}

	return "", fmt.Errorf("host header not found")
}

func (http *HTTP) runProxy() error {
	l, err := net.Listen("tcp", http.bind)

	if err != nil {
		return err
	}
	for {
		conn, err := l.Accept()

		if err != nil {
			return err
		}

		go runHTTPProxyConn(conn)
	}
}

// StartHTTP start an HTTP proxy.
func StartHTTP(bind string) (*HTTP, error) {
	log.Printf("Starting HTTP Proxy on %s.", bind)

	var proxy = HTTP{bind}
	go proxy.runProxy()

	return nil, errors.New("not implemented")
}
