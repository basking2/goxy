package goxy

import (
	"errors"
	"log"
	"net"
)

// ProxyConnMessage represents a single bundle of data traveling to or from a proxy endpoint
type ProxyConnMessage struct {
	Data   []byte
	Length int
	Err    error
}

// ProxyConnFilter defines a way to inspect and transform bytes as they pass between connections.
type ProxyConnFilter func(*ProxyConnMessage) error

// ProxyConn represents a connection between two net.Conn structs that are proxied to one another.
type ProxyConn struct {
	Client        net.Conn
	Server        net.Conn
	ClientFilters []ProxyConnFilter
	ServerFilters []ProxyConnFilter
}

// NewProxyConn is a simple constructor.
func NewProxyConn(client net.Conn, server net.Conn) ProxyConn {
	return ProxyConn{client, server, make([]ProxyConnFilter, 0), make([]ProxyConnFilter, 0)}
}

// AddClientFilter appends a client filter to the list of filters.
func (proxyConn *ProxyConn) AddClientFilter(filter ProxyConnFilter) {
	proxyConn.ClientFilters = append(proxyConn.ClientFilters, filter)
}

// AddServerFilter appends a client filter to the list of filters.
func (proxyConn *ProxyConn) AddServerFilter(filter ProxyConnFilter) {
	proxyConn.ServerFilters = append(proxyConn.ServerFilters, filter)
}

// DoProxy the proxyConn by creating go routines to send and receive data.
// This will block the caller until the connections are closed.
func (proxyConn *ProxyConn) DoProxy() error {

	log.Printf("Beginning proxy session from %s to %s.",
		proxyConn.Client.RemoteAddr().String(), proxyConn.Server.RemoteAddr().String())

	var fromClient = recvData(proxyConn.Client)
	var fromServer = recvData(proxyConn.Server)

	for {
		select {
		case msg := <-fromClient:
			// log.Println("SENDING FROM CLIENT TO SERVER")
			if err := sendData(proxyConn.Client, proxyConn.Server, proxyConn.ClientFilters, msg); err != nil {
				proxyConn.Client.Close()
				proxyConn.Server.Close()
				return err
			}
		case msg := <-fromServer:
			// log.Print("SENDING FROM SERVER TO CLIENT")
			if err := sendData(proxyConn.Server, proxyConn.Client, proxyConn.ServerFilters, msg); err != nil {
				proxyConn.Client.Close()
				proxyConn.Server.Close()
				return err
			}
		}
	}
}

// recvData blocks and reads from src and publishes messages to the channel.
//
// Callers of this function must NOT receive a second message from the returned
// channel until the first message is duplicated or processing is finished.
func recvData(src net.Conn) chan ProxyConnMessage {

	var dst = make(chan ProxyConnMessage)

	go func(dst chan ProxyConnMessage) error {

		// Single allocation of an even length buffer.
		var bufferSize = 10240
		var buffer = make([]byte, bufferSize*2)
		var bufferLeft = buffer[:bufferSize]
		var bufferRight = buffer[bufferSize:]

		for {
			var len int
			var err error

			// Process the left buffer and block until it is picked up by the receiver.
			len, err = src.Read(bufferLeft)
			dst <- ProxyConnMessage{bufferLeft, len, err}
			if err != nil {
				close(dst)
				return err
			}

			// Process the right buffer and block until it is picked up by the receiver.
			len, err = src.Read(bufferRight)
			dst <- ProxyConnMessage{bufferRight, len, err}
			if err != nil {
				close(dst)
				return err
			}
		}
	}(dst)

	return dst
}

func sendData(src net.Conn, dst net.Conn, filters []ProxyConnFilter, msg ProxyConnMessage) error {

	if msg.Err != nil {
		// The source died. Kill the destination connection.
		dst.Close()
		return msg.Err
	}

	for _, filter := range filters {
		filter(&msg)
	}

	for written := 0; written < msg.Length; {
		var w, err = dst.Write(msg.Data[written:msg.Length])
		if err != nil {
			dst.Close()
			return err
		}
		if w < 0 {
			dst.Close()
			return errors.New("error writting to destination")
		}
		written += w
	}

	return nil
}
