package goxy

import (
	"errors"
	"fmt"
	"log"
	"net"
	"strconv"
)

// Socks holds state of the Socks 5 proxy implementation.
type Socks struct {
	bind string
}

func establishUpstreamConnection(conn net.Conn) (net.Conn, error) {

	var buffer = make([]byte, 10240)

	err := ReadExactly(conn, buffer, 2)
	if err != nil {
		return nil, err
	}

	switch buffer[0] {
	case 0x5:
		return socks5Setup(conn, buffer)
	case 0x4:
		return socks4Setup(conn, buffer)
	default:
		return nil, fmt.Errorf("socks version %d is not supported", buffer[0])
	}
}

func socks4Setup(conn net.Conn, buffer []byte) (net.Conn, error) {
	switch buffer[1] {
	case 0x1: // CONNECT
		var err error
		err = ReadExactly(conn, buffer, 6)
		if err != nil {
			return nil, err
		}

		var port = (int(buffer[0]) << 8) + int(buffer[1])
		var ip = net.IPv4(buffer[2], buffer[3], buffer[4], buffer[5]).To4()
		// var clientID string
		_, err = ReadString(conn, buffer)
		if err != nil {
			return nil, err
		}

		// Detect SOCKS 4a etc.
		var addr string
		if ip[0] == 0x00 && ip[1] == 0x00 && ip[2] == 0x00 && ip[3] != 0x00 {
			var domain, err = ReadString(conn, buffer)
			if err != nil {
				return nil, err
			}
			addr = fmt.Sprintf("%s:%d", domain, port)
		} else {
			addr = fmt.Sprintf("%s:%d", ip.String(), port)
		}

		var server net.Conn
		server, err = net.Dial("tcp", addr)
		if err != nil {
			conn.Write([]byte{0x0, 0x5B, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0})
			conn.Close()
			return nil, err
		}

		conn.Write([]byte{0x0, 0x5A, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0})
		return server, nil
	case 0x2: // BIND command.
		conn.Write([]byte{0x0, 0x5B, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0})
		conn.Close()
		return nil, errors.New("client requested BIND which is unsupported")
	case 0x3: // UDP ASSOCIATE command.
		conn.Write([]byte{0x0, 0x5B, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0})
		conn.Close()
		return nil, errors.New("client requested UDP ASSOCIATE which is unsupported")
	default:
		conn.Write([]byte{0x0, 0x5B, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0})
		conn.Close()
		return nil, fmt.Errorf("client requested unsupported command %d", buffer[1])
	}

}

func socks5Setup(conn net.Conn, buffer []byte) (net.Conn, error) {
	// Read in auth methods. We ignore this for now.
	var err = ReadExactly(conn, buffer, int(buffer[1]))
	if err != nil {
		return nil, err
	}

	// Send authentication choice. 0x0 is no authentication.
	_, err = conn.Write([]byte{0x5, 0x0})
	if err != nil {
		return nil, err
	}

	// Read in proxy command. This should fit in our buffer.
	err = ReadExactly(conn, buffer, 4)
	if err != nil {
		return nil, err
	}

	if buffer[0] != 0x5 {
		return nil, fmt.Errorf("received unexpected version in proxy req: %d", buffer[0])
	}

	switch buffer[1] {
	case 0x1: // CONNECT command.
		var addr, err = readAddress(conn, buffer[3])
		if err != nil {
			conn.Write([]byte{0x5, 0x7, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0})
			conn.Close()
			return nil, err
		}

		var server net.Conn
		server, err = net.Dial("tcp", addr)
		if err != nil {
			conn.Write([]byte{0x5, 0x7, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0})
			conn.Close()
			return nil, err
		}

		if err := sendProxyConnectionReady(conn, server); err != nil {
			conn.Write([]byte{0x5, 0x7, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0})
			conn.Close()
			return nil, err
		}

		return server, err
	case 0x2: // BIND command.
		conn.Write([]byte{0x5, 0x7, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0})
		conn.Close()
		return nil, errors.New("client requested BIND which is unsupported")
	case 0x3: // UDP ASSOCIATE command.
		conn.Write([]byte{0x5, 0x7, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0})
		conn.Close()
		return nil, errors.New("client requested UDP ASSOCIATE which is unsupported")
	default:
		conn.Write([]byte{0x5, 0x7, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0})
		conn.Close()
		return nil, fmt.Errorf("client requested unsupported command %d", buffer[1])
	}

}

func sendProxyConnectionReady(clientConn net.Conn, serverConn net.Conn) error {
	var addr = serverConn.LocalAddr().String()
	var host, portStr, err = net.SplitHostPort(addr)
	var port uint64
	port, err = strconv.ParseUint(portStr, 10, 16)
	if err != nil {
		return err
	}

	var portBytes = []byte{byte((port & 0xff00) >> 8), byte(port & 0xff)}

	var ip = net.ParseIP(host)
	if ip == nil {
		// send dns
		clientConn.Write([]byte{0x5, 0x00, 0x00, 0x03})
		clientConn.Write([]byte{byte(len(host))})
		clientConn.Write([]byte(host))
		clientConn.Write([]byte{portBytes[0], portBytes[1]})
		return nil
	} else if ipv4 := ip.To4(); ipv4 != nil {
		// send v4
		clientConn.Write([]byte{0x5, 0x00, 0x00, 0x01, ipv4[0], ipv4[1], ipv4[2], ipv4[3], portBytes[0], portBytes[1]})
		return nil
	} else if ipv6 := ip.To16(); ipv6 != nil {
		// send v6
		clientConn.Write([]byte{0x5, 0x00, 0x00, 0x01,
			ipv6[0],
			ipv6[1],
			ipv6[2],
			ipv6[3],
			ipv6[4],
			ipv6[5],
			ipv6[6],
			ipv6[7],
			ipv6[8],
			ipv6[9],
			ipv6[10],
			ipv6[11],
			ipv6[12],
			ipv6[13],
			ipv6[14],
			ipv6[15],
			portBytes[0], portBytes[1]})
		return nil
	} else {
		return fmt.Errorf("could not encode local address %s", addr)
	}
}

// readAddress returns a string suitable for use in net.Dial based on the type.
func readAddress(conn net.Conn, addressType byte) (string, error) {
	switch addressType {
	case 0x1: // IPv4

		var buffer = make([]byte, 6)
		err := ReadExactly(conn, buffer, 6)
		if err != nil {
			return "", err
		}

		var port = (int(buffer[4]) << 8) + int(buffer[5])
		var addr = fmt.Sprintf("%d.%d.%d.%d:%d", buffer[0], buffer[1], buffer[2], buffer[3], port)
		return addr, nil

	case 0x3: // DNS
		var buffer = make([]byte, 1024)
		err := ReadExactly(conn, buffer, 1)
		if err != nil {
			return "", err
		}

		var nameLen = int(buffer[0])
		err = ReadExactly(conn, buffer, nameLen)
		if err != nil {
			return "", err
		}

		var hostname = string(buffer[0:nameLen])

		err = ReadExactly(conn, buffer, 2)
		if err != nil {
			return "", err
		}

		var port = (int(buffer[0]) << 8) + int(buffer[1])

		return fmt.Sprintf("%s:%d", hostname, port), err

	case 0x4: // IPv6
		var buffer = make([]byte, 18)
		err := ReadExactly(conn, buffer, 18)
		if err != nil {
			return "", err
		}

		var port = (int(buffer[16]) << 8) + int(buffer[17])
		var addr = fmt.Sprintf("[%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d]:%d",
			buffer[0],
			buffer[1],
			buffer[2],
			buffer[3],
			buffer[4],
			buffer[5],
			buffer[6],
			buffer[7],
			buffer[8],
			buffer[9],
			buffer[10],
			buffer[11],
			buffer[12],
			buffer[13],
			buffer[14],
			buffer[15],
			port)
		return addr, nil
	default:
		return "", fmt.Errorf("unsupported address type %d", addressType)
	}

}

func runSocksProxyConn(conn net.Conn) error {
	var server, err = establishUpstreamConnection(conn)

	if err != nil {
		conn.Close()
		return err
	}

	var proxyConn = NewProxyConn(conn, server)

	return proxyConn.DoProxy()
}

func (socks *Socks) runProxy() error {
	l, err := net.Listen("tcp", socks.bind)

	if err != nil {
		return err
	}
	for {
		conn, err := l.Accept()

		if err != nil {
			return err
		}

		go runSocksProxyConn(conn)
	}
}

// StartSocks Creates and starts a new proxy server.
func StartSocks(bind string) (*Socks, error) {
	log.Printf("Starting SOCKS 4c and 5 on %s.", bind)

	var proxy = Socks{bind}
	go proxy.runProxy()

	return &proxy, errors.New("not implemented")
}
