package goxy

// ProxyConfig describes a proxy to run.
type ProxyConfig struct {
	Type string
	Bind string
}

// SocksProxyConfigList is a list of ProxyConfig structs configured for SOCKS.
type SocksProxyConfigList []ProxyConfig

// HTTPProxyConfigList is a list of ProxyConfig structs configured for SOCKS.
type HTTPProxyConfigList []ProxyConfig

// Config holds configuration for Goxy.
type Config struct {
	Socks SocksProxyConfigList
	HTTP  HTTPProxyConfigList
}

// Set will append a value to a strling list.
func (i *HTTPProxyConfigList) Set(value string) error {
	var p = ProxyConfig{"http", value}

	*i = append(*i, p)

	return nil
}

// String will convert a string list into a string.
func (i *HTTPProxyConfigList) String() string {
	var s = "http: "
	for _, v := range *i {
		s += v.Bind + " "
	}
	return s
}

// Set will append a value to a strling list.
func (i *SocksProxyConfigList) Set(value string) error {
	var p = ProxyConfig{"socks", value}

	*i = append(*i, p)

	return nil
}

// String will convert a string list into a string.
func (i *SocksProxyConfigList) String() string {
	var s = "socks: "
	for _, v := range *i {
		s += v.Bind + " "
	}
	return s
}
