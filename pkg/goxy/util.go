package goxy

import "net"

import "fmt"

import "strings"

// ReadExactly reads exactly len bytes into the start of the buffer.
func ReadExactly(conn net.Conn, buffer []byte, len int) error {
	for offset := 0; offset < len; {
		i, err := conn.Read(buffer[offset:len])
		if err != nil {
			return err
		}

		offset += i
	}

	return nil
}

// ReadString reads into the buffer. When a \0 is encountered, the preceding bytes are wrapped and returned as a string.
// The connection is pointing at the first byte after the \0.
func ReadString(conn net.Conn, buffer []byte) (string, error) {
	var s = ""
	var i = 0

	for {
		// Read 1 byte into the buffer.
		var read, err = conn.Read(buffer[i:(i + 1)])
		if err != nil {
			return s, err
		}

		if read > 0 { // We've read anything.
			if buffer[i] == 0x00 { // Last read is null.
				s += string(buffer[0 : i+1])
				return s, nil
			}

			i++

			if i >= len(buffer) { // We've exhausted our buffer.
				s += string(buffer)
				i = 0
			}

			if len(s) > 10240 { // The string is just too long.
				return s, fmt.Errorf("the string read is too long (%d)", len(s))
			}
		}
	}
}

// ReadStringUntilContains reads until the substring given appears.
//
// This is useful for protocols that have a string terminator of some sort that doesn't appear
// in the middle of a message, like HTTP uses \r\n\r\n to signal the end of the header.
//
// The returned string can contain more than just the intended message. In the case of
// HTTP, some of the body might be in the returned string. For use by Goxy
// this is acceptable because in these cases then entire message will be forwarded
// with only the status line modified.
//
// In the case where a string is too long, the string is returned. The caller can then
// decide to keep searching or abort the effort.
func ReadStringUntilContains(conn net.Conn, buffer []byte, suffix string) (string, error) {
	var s = ""

	for {
		var i, err = conn.Read(buffer)
		if err != nil {
			return s, err
		}

		s += string(buffer[0:i])

		if strings.Contains(s, suffix) {
			return s, nil
		}

		if len(s) >= 10240 {
			return s, fmt.Errorf("the string read is too long (%d) and the search string was not found", len(s))
		}
	}
}
